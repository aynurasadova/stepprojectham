const filterTitlesContainer = document.getElementById('filter-titles');
const projects = document.querySelectorAll('.photo-item');

filterTitlesContainer.addEventListener('click',(e) =>{
    if(e.target.classList.contains('tabs-second-title')){
        const title = e.target;
        const type = title.dataset.filterby || 'photo-item';
        const isActive = title.classList.contains('active-second');
        if(!isActive){
            document.querySelector('.tabs-second-title.active-second').classList.remove('active-second');
            title.classList.add('active-second');
            filterByClassName(projects,type);
        }
        console.log(type);
    }
})

function filterByClassName(elements, className) {
    for(let element of elements){
        element.hidden = !element.classList.contains(className);
    }
}

const loadBtn = document.getElementById('load');
const loadPics = document.querySelectorAll('#none-display');

for(let element of loadPics) {
    element.style.display = "none";
}

loadBtn.addEventListener('click',(e) => {
    
    for(let elm of loadPics) {
        elm.style.display = "flex";
    }
    
    loadBtn.style.display = "none";
    

});








