
let tabsTitle = document.querySelectorAll(".tabs-title");
let tabs = document.querySelector('.tabs')
let tabsContent = document.querySelector('.tabs-content')

console.log(tabsTitle);
for (let item of tabsTitle){
    item.addEventListener("click", () => {
        document.querySelector(".active").classList.remove("active");
        item.classList.add("active");
        for (let i = 0; i < tabsContent.children.length; i++){
            if(tabs.children[i].classList.contains("active")){
                document.querySelector(".active-tab").classList.remove("active-tab");
                tabsContent.children[i].classList.add("active-tab");
            }
        }
    })
}